from django import forms
from django.db import models
from .models import Author
from django.forms import ModelForm, TextInput, DateInput, FileInput

class AuthorForm(ModelForm): # Новый класс мы наследуем от ModelForm
    class Meta:     # где указываем характеристики, для класса
        model = Author  # Модель, с которой мы работаем
        fields = ['last_name', 'first_name', 'date_of_birth', 'date_of_death', 'foto'] # Поля, которые должны быть выведены в форме
        widgets = {
            "last_name": TextInput(attrs={  # Словарь attrs содержит HTML атрибуты, которые будут назначены сгенерированному виджету.
                'class': 'form-control',
                'placeholder': "Фамилия"
            }),
            "first_name": TextInput(attrs={
                'class': 'form-control',
                'placeholder': "Имя и Отчество"
            }),
             "date_of_birth": DateInput(attrs={
                'class': 'form-control',
                'placeholder': "Родился"
            }),
            "date_of_death": DateInput(attrs={
                'class': 'form-control',
                'placeholder': "Умер"
            }),
             "foto": FileInput(attrs={
                'class': '',
                'placeholder': "Портрет"
            }),
            }
    

